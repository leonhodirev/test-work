<?php

use yii\db\Migration;
use yii\behaviors\TimestampBehavior;

/**
 * Class m181106_084512_authors
 */
class m181106_084512_author extends Migration
{
    public function up()
    {
        $this->createTable('author', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fx_book_author_id_author', 'book', 'id_author',
            'author', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fx_book_author_id_author', 'book');

        $this->delete('author');
    }
}
