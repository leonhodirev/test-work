<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Главная';
?>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemOptions' => ['class' => 'item'],
    'itemView' => function ($model, $key, $index, $widget) {
        $books = '';
        foreach ($model->books as $book) {
            $books .= ' ' . Html::a(Html::encode($book->name), ['/admin/book/view', 'id' => $model->id]) . '; ';
        }
        $books = !empty($books) ? 'Книги: ' . $books : 'У автора нет книг';
        return Html::a($model->name, ['/admin/author/view', 'id' => $model->id]) . ' - ' . $books;
    },
]) ?>
